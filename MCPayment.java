package test;

import java.util.ArrayList;
import java.util.List;

public class MCPayment {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		System.out.println("The Result number 1 :"+number1());
		System.out.println("The Result number 2 :"+number2());
		System.out.println("The Result number 3 :"+number3());
	}
	
	public static List<Integer>  number1() {
		
		
//		We have array of integers called nums, write a function to return all numbers (in a form
//		of array of integers) that when subtracted by any of integers in nums doesn't return
//		number that is < 0
//		for example : nums = [3,1,4,2], your output should be : [4], because when 4 is subtracted
//		by 3 or 1 or 4 or 2 doesn't return number that is < 0
		
		int[] num = {3,1,4,2};
		List<Integer> add = new ArrayList<Integer>();
		for(int i=0; i < num.length; i++) {
			boolean fail=false;
			for(int j=0; j < num.length; j++) {
				if(i != j) {
					int tmp = num[i]-num[j];
					if(tmp <0 ) {
						fail =true;
						break;
					}
				}
			}
			
			if(!fail) {
				add.add(num[i]);
			}
		}
		
		
		
		
		return add;
	}

	public static List<Integer>  number2() {
		
		
//		We have array of integers called nums and an integer called x, write a function to return
//		all numbers (in a form of array of integers) that when divided by any of integers in nums
//		doesn't return x
//		for example : nums = [1,2,3,4], x = 4, your output should be : [1,2,3], because only 4
//		divided by 1 is 4 (x)
		
		int x = 4;
		int[] num = {1,2,3,4};
		List<Integer> add = new ArrayList<Integer>();
		for(int i=0; i < num.length; i++) {
			int numObj = num[i];
			if((numObj/x) < 1) {
				add.add(num[i]);
			}
		}
		
		
		
		
		return add;
	}
	
	
public static List<String>  number3() {
		
		
//		We have a string called word and an integer called x, write a function to return an array
//		of strings containing all strings that has length x.
//		for example : word = "souvenir loud four lost", x = 4, your output should be ["loud", "four",
//		"lost"] because those strings has length of 4
			
		List<String> add = new ArrayList<String>();
		int x = 4;
		String word = "souvenir loud four lost";
		String[] wordArr = word.split(" ");
		for(int i=0; i < wordArr.length; i++) {
			int wordInt = wordArr[i].length();
			if(wordInt <= x) {
				add.add(wordArr[i]);
			}
		}
		
		
		
		
		return add;
	}
	
	
	
}
